package fizzbuzz;

public class FizzBuzz {

	public static void main(String[] args) {
		for(int i = 1; i <= 100; i++) {
			System.out.println(fizzBuzz(i));
		}
	}
	static String fizzBuzz(int number) {
		final String FIZZBUZZ_WORD = "FizzBuzz";
		final String FIZZ_WORD = "Fizz";
		final String BUZZ_WORD = "Buzz";
		if(number % 15 == 0) {
			return FIZZBUZZ_WORD;
		} else if(number % 3 == 0) {
			return FIZZ_WORD;
		} else if(number % 5 == 0) {
			return BUZZ_WORD;
		} else {
			return String.valueOf(number);
		}
	}
}
