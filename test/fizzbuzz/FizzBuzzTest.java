package fizzbuzz;

import static org.junit.Assert.*;

import org.junit.Test;

public class FizzBuzzTest {

	@Test
	public void testfizz() {
		assertEquals("Fizz", FizzBuzz.fizzBuzz(9));
	}

	@Test
	public void testBuzz() {
		assertEquals("Buzz", FizzBuzz.fizzBuzz(20));
	}
	@Test
	public void testfizzBuzz() {
		assertEquals("FizzBuzz", FizzBuzz.fizzBuzz(45));
	}
	@Test
	public void testNum() {
		assertEquals(String.valueOf(44), FizzBuzz.fizzBuzz(44));
	}
	@Test
	public void testNum2() {
		assertEquals(String.valueOf(46), FizzBuzz.fizzBuzz(46));
	}

}

